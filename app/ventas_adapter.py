from google.cloud import bigquery
from ventas_port import VentasRepositoryPort
from ventas_domain import Venta

class VentasRepositoryBigQuery(VentasRepositoryPort):

    def __init__(self):
        # Inicializa el cliente de BigQuery
        self.client = bigquery.Client()

    def obtener_ventas_mensuales(self):
        # Define tu consulta
        QUERY = (
            'SELECT producto, mes, ventas_mensuales, region FROM `igajardo.ventas.ventas`'
        )
        query_job = self.client.query(QUERY)
        rows = query_job.result()

        # Convierte los resultados en entidades de dominio
        ventas = [Venta(row.producto, row.mes, row.ventas_mensuales, row.region) for row in rows]

        return ventas
