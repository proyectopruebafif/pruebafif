from flask import Flask, jsonify
from flask_cors import CORS
from ventas_adapter import VentasRepositoryBigQuery

app = Flask(__name__)
CORS(app)

@app.route('/api/ventas-mensuales', methods=['GET'])
def obtener_ventas():
    # 1. Crear una instancia de la clase
    repositorio = VentasRepositoryBigQuery()

    # 2. Obtener los resultados
    ventas_mensuales = repositorio.obtener_ventas_mensuales()

    # Convertir las ventas en una lista de diccionarios para ser serializadas a JSON
    # Suponiendo que la clase Venta tiene atributos para producto, fecha, cantidad y región
    ventas_list = [
        {
            "producto": venta.producto,
            "mes": venta.mes,
            "ventas_mensuales": venta.ventas_mensuales,
            "region": venta.region
        }
        for venta in ventas_mensuales
    ]

    # 3. Devolver las ventas como JSON
    return jsonify(ventas_list)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080)