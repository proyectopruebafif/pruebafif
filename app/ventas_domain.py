class Venta:
    def __init__(self, producto, mes, ventas_mensuales, region):
        self.producto = producto
        self.mes = mes
        self.ventas_mensuales = ventas_mensuales
        self.region = region

    def __str__(self):
        return f"Producto: {self.producto}, Mes: {self.mes}, Ventas_mensuales: {self.ventas_mensuales}, Región: {self.region}"