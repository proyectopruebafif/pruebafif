FROM python:3.11.2

WORKDIR /app
COPY /app .

RUN pip config set global.index-url https://pypi.org/simple/
RUN pip install flask
RUN pip install Flask-CORS
RUN pip install google-cloud-bigquery

CMD ["python", "main.py"]
